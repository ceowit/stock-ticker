var chai = require("chai");
var chaiHttp = require("chai-http");
var app = require("../index")

var expect = chai.expect;

chai.use(chaiHttp);

describe("api/stocks",function(){

	it("Get Stock Ticker",function(done){
	
		chai.request(app).get("/api/stocks").end(function(err,res){
			expect(res).to.have.status(200);
			done();
		});

	});

});

